import express from "express";
import axios from "axios";
import "dotenv/config";

const app = express();
const url = process.env.URL_RANDOM_USER;

app.get("/customers", async function (req, res) {
  const response = await axios.get(url);
  if (response.status == 200) {
    const data = response.data.results[0];
    res.status(200).send({
      nombre: data.name.first,
      apellido: data.name.last,
      telefono: data.phone,
    });
  }
  res.status(400);
});

export default app;
