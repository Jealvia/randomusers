# TEST NODEJS

Esta serverless app se invoca cuando un AWS Api Gateway recibe una solicitud GET /customers. Utiliza la API Random User para obtener los datos del usuario (nombre, apellido).

## Usage
Para ejecutar el servidor en el puerto [3000](http://localhost:3000/)
```
npm run start
```
Para ejecutar los tests
```
npm run test
```

## License
[MIT](https://choosealicense.com/licenses/mit/)