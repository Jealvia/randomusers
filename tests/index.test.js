import app from "../src/app";
import request from "supertest";

const api = request(app);

describe("GET /customers", () => {
  const get_customers = api.get("/customers").send();
  test("should respond with a 200 status code", async () => {
    const response = await get_customers;
    expect(response.statusCode).toBe(200);
  });
  test("should respond with a 200 status code / supertest", async () => {
    await api
      .get("/customers")
      .expect(200)
      .expect("Content-Type", /application\/json/);
  });
  test("should respond json data", async () => {
    const response = await get_customers;
    expect(response.body).toBeInstanceOf(Object);
  });
  test("should respond json data with: nombre, apellido y telefono", async () => {
    const response = await get_customers;
    const keys = Object.keys(response.body);
    expect(keys).toEqual(
      expect.arrayContaining(["nombre", "apellido", "telefono"])
    );
  });
});
